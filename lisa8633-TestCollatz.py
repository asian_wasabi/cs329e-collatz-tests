#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, collatz

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read2(self):
        s = "10 100\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 100)

    def test_read3(self):
        s = "6 89\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  6)
        self.assertEqual(j, 89)

    def test_read4(self):
        s = "3478 990123\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  3478)
        self.assertEqual(j, 990123)
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1000, 10)
        self.assertEqual(v, 179)

    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 10, 100, 1000)
        self.assertEqual(w.getvalue(), "10 100 1000\n")

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 4, 67, 98)
        self.assertEqual(w.getvalue(), "4 67 98\n")

    def test_print4(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")
    # -----
    # solve
    # -----

    def testCollatz1(self):
        x = collatz(10)
        self.assertEqual(x, 7)

    def testCollatz2(self):
        x = collatz(100)
        self.assertEqual(x, 26)

    def testCollatz(self):
        x = collatz(88)
        self.assertEqual(x, 18)

    def test_solve1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve2(self):
        r = StringIO("9 10\n1 2\n20 100\n1 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "9 10 20\n1 2 2\n20 100 119\n1 1 1\n")

    def test_solve3(self):
        r = StringIO("10 10\n900 1000\n34 89\n2 2\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 10 7\n900 1000 174\n34 89 116\n2 2 2\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
